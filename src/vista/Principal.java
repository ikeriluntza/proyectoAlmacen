package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorArticulo;
import controlador.ControladorCliente;
import controlador.ControladorPedido;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JButton gestionArticulo;
	private JButton gestionCliente;
	private JButton gestionPedido;

	private ControladorArticulo controladorArticulo;
	private ControladorCliente controladorCliente;
	private ControladorPedido controladorPedido;

	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}

	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}

	public ControladorCliente getControladorCliente() {
		return controladorCliente;
	}

	public void setControladorCliente(ControladorCliente controladorCliente) {
		this.controladorCliente = controladorCliente;
	}

	public ControladorPedido getControladorPedido() {
		return controladorPedido;
	}

	public void setControladorPedido(ControladorPedido controladorPedido) {
		this.controladorPedido = controladorPedido;
	}

	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		gestionArticulo = new JButton("Gesti\u00F3n Articulo");
		gestionArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				controladorArticulo.abrirGestionArticulo();
			}
		});
		gestionArticulo.setBounds(124, 92, 150, 23);
		contentPane.add(gestionArticulo);

		gestionCliente = new JButton("Gesti\u00F3n Cliente");
		gestionCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				controladorCliente.abrirGestionCliente();
			}
		});
		gestionCliente.setBounds(124, 145, 150, 23);
		contentPane.add(gestionCliente);

		gestionPedido = new JButton("Gesti\u00F3n Pedido");
		gestionPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				controladorPedido.abrirGestionPedido();
			}
		});
		gestionPedido.setBounds(124, 198, 150, 23);
		contentPane.add(gestionPedido);

		JLabel lblNewLabel = new JLabel("Proyecto Almacen");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(139, 27, 135, 14);
		contentPane.add(lblNewLabel);
	}
}
