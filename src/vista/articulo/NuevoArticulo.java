package vista.articulo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorArticulo;


import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NuevoArticulo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nombre;
	private JTextField proveedor;
	private JTextField precio;
	private JTextField existencias;

	private ControladorArticulo controladorArticulo;



	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}



	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}



	/**
	 * Create the dialog.
	 */
	public NuevoArticulo(JDialog parent,boolean modal) {
		
		super(parent,modal);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(136, 48, 136, 20);
		contentPanel.add(nombre);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(37, 51, 46, 14);
		contentPanel.add(lblNombre);
		
		proveedor = new JTextField();
		proveedor.setColumns(10);
		proveedor.setBounds(136, 104, 136, 20);
		contentPanel.add(proveedor);
		
		JLabel lblProveedor = new JLabel("Proveedor");
		lblProveedor.setBounds(37, 107, 73, 14);
		contentPanel.add(lblProveedor);
		
		precio = new JTextField();
		precio.setColumns(10);
		precio.setBounds(136, 160, 136, 20);
		contentPanel.add(precio);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(37, 163, 90, 14);
		contentPanel.add(lblPrecio);
		
		JButton button = new JButton("GUARDAR");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controladorArticulo.insertarArticulo(nombre.getText(), proveedor.getText(), 
						Double.parseDouble(precio.getText()), Integer.parseInt(existencias.getText()));
			}
		});
		button.setBounds(298, 129, 106, 23);
		contentPanel.add(button);
		
		JLabel lblAltaDeArticulo = new JLabel("ALTA DE ARTICULO");
		lblAltaDeArticulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblAltaDeArticulo.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAltaDeArticulo.setBounds(55, 11, 300, 14);
		contentPanel.add(lblAltaDeArticulo);
		
		JButton button_1 = new JButton("LIMPIAR");
		button_1.setBounds(298, 189, 106, 23);
		contentPanel.add(button_1);
		
		JLabel lblExistencias = new JLabel("Existencias");
		lblExistencias.setBounds(37, 219, 90, 14);
		contentPanel.add(lblExistencias);
		
		existencias = new JTextField();
		existencias.setColumns(10);
		existencias.setBounds(136, 216, 136, 20);
		contentPanel.add(existencias);
	}



	
}
