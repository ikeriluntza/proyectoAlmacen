package vista.articulo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controlador.ControladorArticulo;
import modelo.Articulo;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ConsultarArticulo extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private ControladorArticulo controladorArticulo;
	private JTable tabla;

	
	
	
	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}



	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}



	public ConsultarArticulo(JDialog parent, boolean modal) {
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(34, 50, 359, 178);
		getContentPane().add(scrollPane);
		
		tabla = new JTable();
		scrollPane.setColumnHeaderView(tabla);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(37, 37, 359, 178);
		getContentPane().add(scrollPane);

		super(parent, modal);
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblConsultasDeArticulos = new JLabel(CONSULTAS DE ARTICULOS);
		lblConsultasDeArticulos.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsultasDeArticulos.setFont(new Font(Tahoma, Font.BOLD, 18));
		lblConsultasDeArticulos.setBounds(71, 22, 300, 14);
		contentPanel.add(lblConsultasDeArticulos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 57, 359, 178);
		contentPanel.add(scrollPane);
		
		tabla = new JTable();
		tabla.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		scrollPane.setViewportView(tabla);
	}
	
	public void rellenarTabla(ArrayList<Articulo> articulos) {
//		 cargar la tabla
		DefaultTableModel dtm = new DefaultTableModel();

		String [] encabezados={ NOMBRE, PROVEEDOR, PRECIO, EXISTENCIAS };
		
		dtm.setColumnIdentifiers(encabezados);
		for (Articulo articulo  articulos) {
			String[] fila={ articulo.getNombre(), articulo.getProveedor(), String.valueOf(articulo.getPrecio()), String.valueOf(articulo.getExistencias())  };
			dtm.addRow(fila);
		}
		tabla.setModel(dtm);
		TableRowSorterDefaultTableModel modeloOrdenado = new TableRowSorterDefaultTableModel(dtm);
		tabla.setRowSorter(modeloOrdenado);

	}
}