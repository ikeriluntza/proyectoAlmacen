package vista.articulo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorArticulo;
import modelo.Articulo;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class BorrarArticulo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField id;
	private JTextField nombre;
	private JTextField proveedor;
	private JTextField precio;
	private JTextField existencias;

	private ControladorArticulo controladorArticulo;
	
	private JComboBox lista;
	private JLabel lblIdArticulo;
	private JLabel lblNombre;
	private JLabel lblProveedor;
	private JLabel lblPrecio;
	private JButton button;
	private JLabel lblExistencias;

	

	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}



	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}



	/**
	 * Create the dialog.
	 */
	public BorrarArticulo(JDialog parent, boolean modal) {
		
		super(parent,modal);
		setBounds(100, 100, 433, 322);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		lista = new JComboBox();
		lista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				controladorArticulo.seleccionarDatosArticulo((String)lista.getSelectedItem());
			}
		});
		lista.setBounds(86, 11, 231, 36);
		contentPanel.add(lista);
		
		id = new JTextField();
		id.setEditable(false);
		id.setColumns(10);
		id.setBounds(119, 64, 163, 20);
		contentPanel.add(id);
		
		lblIdArticulo = new JLabel("Id articulo");
		lblIdArticulo.setBounds(23, 67, 72, 14);
		contentPanel.add(lblIdArticulo);
		
		nombre = new JTextField();
		nombre.setEditable(false);
		nombre.setColumns(10);
		nombre.setBounds(119, 110, 163, 20);
		contentPanel.add(nombre);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(23, 113, 46, 14);
		contentPanel.add(lblNombre);
		
		proveedor = new JTextField();
		proveedor.setEditable(false);
		proveedor.setColumns(10);
		proveedor.setBounds(119, 156, 163, 20);
		contentPanel.add(proveedor);
		
		lblProveedor = new JLabel("Proveedor");
		lblProveedor.setBounds(23, 159, 58, 14);
		contentPanel.add(lblProveedor);
		
		precio = new JTextField();
		precio.setEditable(false);
		precio.setColumns(10);
		precio.setBounds(119, 202, 163, 20);
		contentPanel.add(precio);
		
		lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(23, 205, 90, 14);
		contentPanel.add(lblPrecio);
		
		button = new JButton("BORRAR");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				controladorArticulo.borrarArticulo((String)lista.getSelectedItem());
				lista.setSelectedIndex(0);
				controladorArticulo.abrirBorrarArticulo();
				
			}
		});
		button.setBounds(312, 223, 89, 23);
		contentPanel.add(button);
		
		lblExistencias = new JLabel("existencias");
		lblExistencias.setBounds(23, 251, 90, 14);
		contentPanel.add(lblExistencias);
		
		existencias = new JTextField();
		existencias.setEditable(false);
		existencias.setColumns(10);
		existencias.setBounds(119, 248, 163, 20);
		contentPanel.add(existencias);
	}

	
	public void rellenarLista(ArrayList<Articulo> articulos) {
		
		lista.removeAllItems();
		
		for (Articulo articulo:articulos){
			
			lista.addItem(articulo.getNombre());
		}
	}
	
	public void mostrarDatos(Articulo articulo) {
		
		id.setText(String.valueOf(articulo.getId()));
		nombre.setText(articulo.getNombre());
		proveedor.setText(articulo.getProveedor());
		precio.setText(String.valueOf(articulo.getPrecio()));
		existencias.setText(String.valueOf(articulo.getExistencias()));
	}

	public void limpiar() {
		lista.setSelectedIndex(0);
	}

}
