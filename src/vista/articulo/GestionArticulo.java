package vista.articulo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorArticulo;
import vista.Principal;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionArticulo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton nuevoArticulo;
	private JButton borrarArticulo;
	private JButton consultarArticulo;
	private JButton listarArticulo;

	private ControladorArticulo controladorArticulo;
	
	
	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}


	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}

	/**
	 * Create the dialog.
	 */
	public GestionArticulo(Principal parent, boolean modal) {
		
		super(parent,modal);
		
		setBounds(100, 100, 251, 274);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		nuevoArticulo = new JButton("Nuevo Articulo");
		nuevoArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				controladorArticulo.abrirNuevoArticulo();
			}
		});
		nuevoArticulo.setBounds(39, 28, 163, 23);
		contentPanel.add(nuevoArticulo);
		
		borrarArticulo = new JButton("Borrar Articulo");
		borrarArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				controladorArticulo.abrirBorrarArticulo();
			}
		});
		borrarArticulo.setBounds(39, 79, 163, 23);
		contentPanel.add(borrarArticulo);
		
		consultarArticulo = new JButton("Consultar Articulo");
		consultarArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controladorArticulo.abrirConsultarArticulo();
			}
		});
		consultarArticulo.setBounds(39, 130, 163, 23);
		contentPanel.add(consultarArticulo);
		
		listarArticulo = new JButton("Listar Articulo");
		listarArticulo.setBounds(39, 181, 163, 23);
		contentPanel.add(listarArticulo);
	}

}
